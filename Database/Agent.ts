let Agents = [];

for (let id = 1; id <= 100; id++) {
  Agents.push({
    id,
    name: `Agent ${id}`
  })
}

export default Agents;