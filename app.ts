import * as DB from "./Database";
import {RandomNumber} from "./Helper";



// CODE HERE














interface iEvent {
  id: number;
  event: "RINGING" | "ANSWER" | "HANGUP";
  queue_id: number;
  agent_id?: string;
}

const eventHandler = (payload: iEvent) => {
  console.log(payload)
};

const WebsocketEmitter = console.log;

// DO NOT CHANGE
const CallSimulator = () => {
  let CallID = 0;
  const TOTAL_INDEX = DB.AgentQueues.length;
  setInterval(() => {
    const AgentQueue = DB.AgentQueues[RandomNumber(TOTAL_INDEX)];
    let payload: iEvent = {
      id: CallID++,
      event: "RINGING",
      queue_id: AgentQueue.queue_id
    };

    eventHandler(payload);

    let TIMEOUT = RandomNumber(5, 1);

    setTimeout(() => {
      payload.event = "ANSWER";
      payload.agent_id = AgentQueue.agent_id;
      eventHandler(payload);
    }, TIMEOUT * 1e3);

    TIMEOUT += RandomNumber(15, 5);
    setTimeout(() => {
      payload.event = "HANGUP";
      payload.agent_id = AgentQueue.agent_id;
      eventHandler(payload)
    }, TIMEOUT * 1e3);


  }, 250);
};

CallSimulator();