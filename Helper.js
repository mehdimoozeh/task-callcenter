"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RandomNumber = function (end, start) {
    if (start === void 0) { start = 0; }
    return Math.floor(Math.random() * end) + start;
};
