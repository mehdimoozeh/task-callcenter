import Agents from "./Database/Agent";
import Queues from "./Database/Queue";
import AgentQueues from "./Database/AgentQueue";

export {
  Agents,
  Queues,
  AgentQueues
}